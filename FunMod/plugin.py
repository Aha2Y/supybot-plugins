###
# Copyright (c) 2012, Aha2Y
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from supybot.i18n import PluginInternationalization, internationalizeDocstring
import urllib2
import lxml.html
import random

_ = PluginInternationalization('FunMod')

@internationalizeDocstring
class FunMod(callbacks.Plugin):
    """Add the help for "@plugin help FunMod" here
    This should describe *how* to use this plugin."""
    threaded = True

    def kill(self, irc, msg, args, victim):
        """nick>
        KILL!!!, Murder the user! Omg! BURN!!"""    
        irc.reply("kills %s for atleast %s times!"  % (victim, random.randint(1, 100)), action=True)
    kill = wrap(kill, ['text'])  
    
    def lick(self, irc, msg, args, victim):
        """<nick>
        Lick a user \o/"""    
        irc.reply("licks %s for atleast %s times!" % (victim, random.randint(1, 100)), action=True)
    lick = wrap(lick, ['text'])  
    
    def cookie(self, irc, msg, args, victim):
        """<nick>
        Give a cookie <3"""    
        irc.reply("gives %s a cookie!!! " % (victim), action=True)
    cookie = wrap(cookie, ['text'])  

    def troll(self, irc, msg, args, victim):
        """<nick>
        Trololol!"""    
        irc.reply("is trolling %s!!! " % (victim), action=True)
    troll = wrap(troll, ['text'])  
    
    def eat(self, irc, msg, args, victim):
        """<nick>
        Omnononononmnonm"""    
        irc.reply("eats %s nomnomnonmn!" % (victim), action=True)
    eat = wrap(eat, ['text'])  
    
    def kickass(self, irc, msg, args, victim):
        """<nick>
        Kick his ass!"""    
        irc.reply("kicks %s's ass for atleast %s times" % (victim, random.randint(1,100)), action=True)
    kickass = wrap(kickass, ['text'])  
    
    def rape(self, irc, msg, args, victim):
        """<nick>
        RAPE HIM!"""    
        irc.reply("Is Raping %s" % (victim), action=True)
    rape = wrap(rape, ['text'])  
    
Class = FunMod


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
