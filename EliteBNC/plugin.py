###
# Copyright (c) 2012, Aha2Y
# Credits Digital_Lemon, Dunj3
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import os
import sys
import time
import socket
import threading
import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from supybot.i18n import PluginInternationalization, internationalizeDocstring

_ = PluginInternationalization('EliteBNC')

SERVERS = ["alpha.elitebnc.net",   "delta.elitebnc.net",
           "epsilon.elitebnc.net", "gamma.elitebnc.net",
           "omega.elitebnc.net",   "rho.elitebnc.net",
           "sigma.elitebnc.net",      "xi.elitebnc.net",
           "zeta.elitebnc.net"]
    
class eBNC:
    def __init__(self):
        self.cache = {}
        self.last = 0
    
    def get(self):
        delta = time.time() - self.last
        if delta < 120:
            return self.cache
        c = []
        for server in SERVERS:
            try:
                srvName = server.split(".elitebnc.net")[0].title()
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                sock.settimeout(01)
                sock.connect((server, 1337))
                sock.shutdown(0)
                c.append((srvName, "3UP"))
            except socket.error,e:
                c.append((srvName, "4DOWN"))
        
        self.cache = c
        self.last = time.time()
        return c
           
@internationalizeDocstring
class EliteBNC(callbacks.Plugin):
    """Add the help for "@plugin help Test" here
    This should describe *how* to use this plugin."""
    threaded = True
    
    def __init__(self, irc):
        self.__parent = super(EliteBNC, self)
        self.__parent.__init__(irc)
        self.status = eBNC()
    
    def elitebnc(self, irc, msg, args):
        irc.reply("Fetching...", prefixNick=False)
        s = self.status.get()
        m = ["%s: %s"% i for i in s]
        msg = ' - '.join(m)        
        irc.reply(msg , prefixNick=False)
    elitebnc = wrap(elitebnc)
     
Class = EliteBNC
 
 
# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79: