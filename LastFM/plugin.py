###
# Copyright (c) 2013, snowkat
# All rights reserved.
#
#
###
import local.pylast as pylast
import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from supybot.i18n import PluginInternationalization, internationalizeDocstring

_ = PluginInternationalization('LastFM')

@internationalizeDocstring
class LastFM(callbacks.Plugin):
	"""Add the help for "@plugin help LastFM" here
	This should describe *how* to use this plugin."""
 	threaded = True

 	#Ugly global until i know a better way..
 	global auth
 	auth = ["user", "password"]

	def np(self, irc, msg, args, user):
		"""<LastFM Profile name>"""
		network = pylast.LastFMNetwork(api_key = self.registryValue("apikey"), api_secret = self.registryValue("apisecret"), username = auth[0], password_hash = pylast.md5(auth[1]))
		if user:
			usr = user
		else:
			usr = msg.nick
		profile = network.get_user(usr)
		if profile.get_now_playing():
			irc.reply("%s is currently playing: %s" % (usr, profile.get_now_playing()))
		else:
			irc.reply("%s is not playing a track at the moment." % (usr))
	np = wrap(np, ['text']) 

Class = LastFM


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
