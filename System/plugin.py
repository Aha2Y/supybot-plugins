# Copyright (c) 2012, Aha2Y 
# All rights reserved.


import subprocess
import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from supybot.i18n import PluginInternationalization, internationalizeDocstring

_ = PluginInternationalization('System')

@internationalizeDocstring
class System(callbacks.Plugin):
    """Add the help for "@plugin help Stats" here
    This should describe *how* to use this plugin."""
    threaded = True

    def ram(self, irc, msg, args):
        if irc.nested: return
        """tells you the ram of the current machine Supybot uses."""
        p = subprocess.Popen(["free", "-o"], stdout=subprocess.PIPE)
        out, err = p.communicate()
        out = out.split()
        total = float(out[7])/1024.0/1024.0
        using = float(out[8])/1024.0/1024.0
        free  = float(out[9])/1024.0/1024.0
        msg = "Total: {1:4.1f} GB | Used:04 {2:4.1f} GB ({3:>6.2%}) | Free:03 {4:4.1f} GB ({5:>6.2%})"
        red_length = int(round((using/total) * 62.0))
        green_length = 62 - red_length
        bar = "[\0034%s\0033%s\x0f]" % ("|" * red_length, "|" * green_length)
        irc.reply(msg.format("", total, using, using/total, free, free/total), prefixNick=False)
        irc.reply(bar, prefixNick=False)
    ram = wrap(ram)
    
    def swap(self, irc, msg, args):
        if irc.nested: return
        """tells you the ram of the current machine Supybot uses."""
        p = subprocess.Popen(["free", "-o"], stdout=subprocess.PIPE)
        out, err = p.communicate()
        out = out.split()
        total = float(out[14])/1024.0/1024.0
        using = float(out[15])/1024.0/1024.0
        free = float(out[16])/1024.0/1024.0
        msg = "Total: {1:4.1f} GB | Used:04 {2:4.1f} GB ({3:>6.2%}) | Free:03 {4:4.1f} GB ({5:>6.2%})"
        red_length = int(round((using/total) * 62.0))
        green_length = 62 - red_length
        bar = "[\0034%s\0033%s\x0f]" % ("|" * red_length, "|" * green_length)
        irc.reply(msg.format("", total, using, using/total, free, free/total), prefixNick=False)
        irc.reply(bar, prefixNick=False)
    swap = wrap(swap)
 
Class = System


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
