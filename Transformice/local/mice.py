import urllib2
import lxml.html

def player_by_name(name):
    name = urllib2.quote(name)
    api = urllib2.urlopen('http://statapi.transformice.com/miceinfo.xml?n=%s' % name)
    data = lxml.html.parse(api)
    api.close()
    return Transformice(data)

class Transformice:
    def __init__(self, xml):
        try:
            err = xml.find('//mice/error').text
            raise TransformiceError(err)
        except AttributeError:
            # Easiest && fastest way to check.
            pass

        self.name         = xml.find('//mice/name').text
        self.tribe        = xml.find('//mice/tribe').text
        self.rounds       = xml.find('//mice/rounds').text
        self.cheese       = xml.find('//mice/cheese').text
        self.first        = xml.find('//mice/first').text
        self.chamansave   = xml.find('//mice/chamansave').text
        self.chamancheese = xml.find('//mice/chamancheese').text
        self.chamangold   = xml.find('//mice/chmangold').text
        self.micetitle    = xml.find('//mice/micetitle').text
    
class TransformiceError(Exception):
    pass
    
