###
# Copyright (c) 2012, Aha2Y
# All rights reserved.
#
#
###
import json
import base64
import requests
import urllib2
from local import mice
from pprint import pprint
import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from supybot.i18n import PluginInternationalization, internationalizeDocstring

_ = PluginInternationalization('Transformice')

@internationalizeDocstring
class Transformice(callbacks.Plugin):
    """Add the help for "@plugin help Transformice" here
    This should describe *how* to use this plugin."""
    threaded = True

    def tfm(self, irc, msg, args, target, text):
        """-m <mice>, -t <tribe>"""
        if text:
            if target == "-m":
                try: 
                    player = mice.player_by_name(text)
                    title = titlelist[player.micetitle]
                    msg = "name: {1} :: title: {2} :: tribe: {3} :: rounds: {4} :: cheese: {5} :: first: {6} (saves: {7} :: cheese: {8})"
                    irc.reply(msg.format("", player.name, title, player.tribe, player.rounds, player.cheese, player.first, player.chamansave, player.chamancheese), prefixNick=False)
                except mice.TransformiceError, e: irc.error(str(e), Raise=True)
            elif target == "-t":
                name = text.encode('base64').strip()
                data = json.load(urllib2.urlopen('http://api.formice.com/tribe/stats.json?t=' + name))
                data['text'] = text
                irc.reply("tribe: %(text)s :: rank: %(rank)s members: %(members)s :"
                ": rounds: %(rounds)s :: cheese: %(cheese)s :: first: %(first)s "
                "(saves: %(saves)s (rounds: %(rounds_rank)s :: cheese: "
                "%(cheese_rank)s :: saves %(saves_rank)s :: first: %(first_rank)s))" % (data), prefixNick=False)  
        else: 
            irc.reply("Unknown flag type.")
    tfm = wrap(tfm, ["something", optional("text")])
Class = Transformice

titlelist = {
            '0': 'Little Mouse',
            '1': 'Shaman Disciple',
            '2': 'Accomplished Shaman',
            '3': 'Shaman',
            '4': 'Shaman Master',
            '5': 'Greedy Mouse',
            '6': 'Here! Cheese!',
            '7': 'Yeeeeah Cheese ^^',
            '8': 'Cheeeeeese *-*',
            '9': 'Fast Mouse',
            '10': 'Agile Mouse',
            '11': 'Pirate Mouse',
            '12': 'Ninja Mouse',
            '13': 'Inspired Shaman',
            '14': 'Shaman Champion',
            '15': 'Glorious Shaman',
            '16': 'Shaman Duchess',
            '17': 'Shaman Princess',
            '18': 'Shaman Empress',
            '19': 'Legendary Shaman',
            '20': 'Immortal Shaman',
            '21': 'The Chosen Shaman',
            '22': 'Holy Shaman',
            '23': 'Shaman Oracle',
            '24': 'Shaman Prophet',
            '25': 'Shamarvelous',
            '26': 'Glutton Mouse',
            '27': 'Gleany',
            '28': 'Plumpy Mouse',
            '29': 'Paunchy Mouse',
            '30': 'Chubby Mouse',
            '31': 'Fluffy Mouse',
            '32': 'Tubby Mouse',
            '33': 'The Chubby',
            '34': 'The Puffy',
            '35': 'Activist Mouse',
            '36': 'Unionized Mouse',
            '37': 'Mouse on Strike',
            '38': 'The Cheese Initiated',
            '39': 'The Cheese Adept',
            '40': 'The Cheese Priest',
            '41': 'The Reaper',
            '42': 'Rogue Mouse',
            '43': 'Looter',
            '44': 'Stalker',
            '45': 'Frothy Mouse',
            '46': 'The Silent',
            '47': 'Hawk Mouse',
            '48': 'Cobra Mouse',
            '49': 'Spidermouse',
            '50': 'Quick Silver',
            '51': 'Athletic Mouse',
            '52': 'Hasty Mouse',
            '53': 'Rocket Mouse',
            '54': 'Sonic The Mouse',
            '55': 'Pingless',
            '56': 'Kamikaze',
            '57': 'Warrior Mouse',
            '58': 'Mach 1',
            '59': 'Hunter',
            '60': 'First!',
            '61': 'Sniper',
            '62': 'Flash',
            '63': 'Supermouse',
            '64': 'Light Speed',
            '65': 'Time Traveler',
            '66': 'Fast Wind',
            '67': 'E=MouseC2',
            '68': 'Jumper',
            '69': 'The Untouchable',
            '70': 'Wall-Jumper',
            '71': 'LIGHTNING',
            '72': 'Cheese Finder',
            '73': 'Cheese Knight',
            '74': 'Cheesegrubber',
            '75': 'Fatty',
            '76': 'Stout Mouse',
            '77': 'Cheese Lover',
            '78': 'Camembert',
            '79': "Pont-L'eveque",
            '80': 'Cheese Catcher',
            '81': "It's Over 9000",
            '82': 'Collector',
            '83': 'Cheeseleader',
            '84': 'Cheese Thief',
            '85': 'Cheese Creator',
            '86': 'Cheese Pizza',
            '87': 'Cheese Minister',
            '88': 'Prodigy Mouse',
            '89': 'Princess of Transformice',
            '90': 'Cheesoholic',
            '91': 'The Cheesen One',
            '92': 'Sailor Mouse',
            '93': 'MAH CHEESE!',
            '94': 'Ancient Shaman',
            '95': 'Fearless Shaman',
            '96': 'Almighty Shaman',
            '97': 'Architect Shaman',
            '98': 'Mademoiselle',
            '99': 'Lady Shaman',
            '100': 'Loved',
            '101': 'Magician',
            '102': 'Hero of Mice',
            '103': 'Angel Shaman',
            '104': 'The Creator',
            '105': 'Absolute Shaman',
            '106': 'Miraculous Shaman',
            '107': 'Liberator',
            '108': 'Troll Shaman',
            '109': 'Ghost Shaman',
            '110': 'Spirit',
            '111': 'Demigodess Shaman',
            '112': 'Last Hope',
            '113': 'Redeemer',
            '114': 'Alpha & Omega',
            '115': 'Nice Mouse',
            '116': 'Adorable Mouse',
            '117': 'Charming Mouse',
            '118': 'Pretty Mouse',
            '119': 'Cute Mouse',
            '120': 'Frivolous Mouse',
            '121': 'Snob Mouse',
            '122': 'Stylish Mouse',
            '123': 'Actress Mouse',
            '124': 'Fashion Mouse',
            '125': 'Sexy',
            '126': 'SuperStar',
            '127': 'Little Snowflake',
            '128': 'Christmas Spirit',
            '129': 'Little Pixie',
            '130': 'Santa Claus',
            '200': 'Goddess Shaman',
            '202': 'The Wind Master',
            '210': 'Alluring Mouse',
            '211': 'Temptress',
            '212': 'Latin Lover',
            '213': 'Decorator',
            '214': 'Builder',
            '215': 'Manufacturer',
            '216': 'Technician',
            '217': 'Mechanic',
            '218': 'Specialist',
            '219': 'Inventor',
            '220': 'Engineer',
            '221': 'Inventive Mouse',
            '222': 'Ingenious Mouse',
            '223': 'Virtuoso',
            '224': 'Sprinter',
            '225': 'Batmouse',
            '226': 'The Unseen',
            '227': 'Unstoppable',
            '228': 'Andale! Andale!',
            '229': 'Torpedo',
            '230': 'Speedy Gorgonzola',
            '231': 'Dynamite',
            '232': 'Speedmaster',
            '233': 'Whirlwind',
            '234': 'Om Nom Nom',
            '235': '*-*',
            '236': 'Cheese Addict',
            '237': 'Cheesus',
            '238': 'Queen of Cheese',
            '240': 'Cookies',
            '241': 'Christmas Cake',
            '242': 'Whitebeard',
            '243': 'Generous',
            '244': 'Snowy',
            '245': 'Snowstorm',
            '246': 'Dauphine',
            '247': 'Foxy',
            '248': 'Miss Transformice'
}

# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
