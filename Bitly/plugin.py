###
# Copyright (c) 2012, Aha2Y
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###
import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from supybot.i18n import PluginInternationalization, internationalizeDocstring
import local.core

_ = PluginInternationalization('Bitly')

@internationalizeDocstring
class Bitly(callbacks.Plugin):
    """<URL>
    Turns the URL into a Bitly short link.
    """
    threaded = True

    def bitly(self, irc, msg, args, url):
        """<url>
        Shorts a long URL into a short URL."""
        try:
           api = local.core.Api(login=self.registryValue("apiLogin"), apikey=self.registryValue("apiKey")) 
           short=api.shorten(url,{'history':1})
           if len(url) > len(short):
               irc.reply("%s -> %s (Shorter: yes)" % (url, short))
           else:
               irc.reply("%s -> %s (Shorter: no)" % (url, short))
        except AttributeError:
            irc.reply("Uh oh, Something must have been wrong...")             
        except TypeError:
            irc.reply("Uh oh, Something must have been wrong...")                  
    bitly = wrap(bitly, ['text'])

    def stats(self, irc, msg, args, url):
        """<url>
        Returns the statistics of a Bitly url."""
        api = local.core.Api(login=self.registryValue("apiLogin"), apikey=self.registryValue("apiKey")) 
        stats=api.stats(url)
        irc.reply("User clicks %s, total clicks: %s" % (stats.user_clicks,stats.total_clicks))
    stats = wrap(stats, ['text'])

    def expand(self, irc, msg, args, url):
        """<url>
        Expands a existing Bitly URL into the real URL."""    
        try:
            api = local.core.Api(login=self.registryValue("apiLogin"), apikey=self.registryValue("apiKey")) 
            long=api.expand(url)
            irc.reply("%s = %s" % (url, long))
        except TypeError:
            irc.reply("Uh oh, Something must have been wrong...")                
    expand = wrap(expand, ['text'])    
    
Class = Bitly