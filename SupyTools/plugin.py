###
# Copyright (c) 2012, Aha2Y
# All rights reserved.
###

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from supybot.i18n import PluginInternationalization, internationalizeDocstring

_ = PluginInternationalization('SupyTools')

@internationalizeDocstring
class SupyTools(callbacks.Plugin):
    """Add the help for "@plugin help SupyTools" here
    This should describe *how* to use this plugin."""
    threaded = True

#Implying feature.
    def doPrivmsg(self, irc, msg):
        message = msg.args[1]
        if self.registryValue("implying", msg.args[0]) and message[0] == ">":
            ignore_characters = ["_", "-", "", ""] 
            if len(message) == 1: return
            if message in ignore_characters: return 
            message = message[1:].strip()
            irc.reply(">implying %s" % (message), prefixNick=False)
        else:
            self.log.info("The implying feature is disabled, You can enable it with changing the config variable to True")
            self.log.info("SET supybot.plugins.SupyTools.implying True")

    def supytools(self, irc, msg, args):
        message = msg.args[1]
        first = "{0:20.10} {1:10} {2:10}"
        msg = "{0:18} {1:5} {2:6}"
        irc.reply(first.format("Plugin", "|", "Status"), prefixNick=False)
        irc.reply(msg.format("Implying", "|", self.registryValue("implying")), prefixNick=False)
    supytools = wrap(supytools)

Class = SupyTools


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
